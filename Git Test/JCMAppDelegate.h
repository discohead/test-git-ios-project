//
//  JCMAppDelegate.h
//  Git Test
//
//  Created by Jared McFarland on 10/14/13.
//  Copyright (c) 2013 Jared McFarland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JCMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
